/**************************************************
 * .cpp file for BMP Dll
 * Has loading and saving functionality of the BMP
 * Can load the BMP and also can load the BMP
 * Samuel Kenney 11/3/2016
 * Using BMP Verion 3
****************************************************/

#include "BMP_Handler.h"
#include <iostream>
#include <fstream>

//version 3 headers

// version 3 file header
typedef struct BMPfileHeader {
	unsigned short  bfType;
	unsigned long   bfSize;
	unsigned short  bfReserved1;
	unsigned short  bfReserved2;
	unsigned long   bfOffBits;
};

//version 3 core header
typedef struct BMPCoreHeader {
	unsigned long   biSize;
	long   biWidth;
	long   biHeight;
	unsigned short  biPlanes;
	unsigned short  biBitCount;
	unsigned long   biCompression;
	unsigned long   biSizeImage;
	long   biXPelsPerMeter;
	long   biYPelsPerMeter;
	unsigned long   biClrUsed;
	unsigned long   biClrImportant;
};

//struct to hold pixels
typedef struct pixelValues {
	unsigned char blue;
	unsigned char green;
	unsigned char red;
};

unsigned char* BMP_Handler::loadBMP(const char* BMP, int& width, int& height){
	std::ifstream fin(BMP, std::ios::binary);

	//fills file Header struct
	BMPfileHeader fileHeader;
	fin.read(reinterpret_cast<char*>(&fileHeader), sizeof(BMPfileHeader));

	//fills Core Header struct
	BMPCoreHeader CoreHeader;
	fin.read(reinterpret_cast<char*>(&CoreHeader), sizeof(BMPCoreHeader));

	//assigns width and height
	width = CoreHeader.biWidth;
	height = CoreHeader.biHeight;

	//makes sure that padding is taken care of
	int padding = 4 - (width*3)%4;
	if (padding == 4) 
		padding = 0;
	int numBytes = 0;

	//creates unsigned char* to hold all of the pixels
	pixelValues BGR;
	unsigned char* pixels;
	pixels = new unsigned char [(width*height)*3];
	int posCursor = 0;

	//fille the array with the bytes from the pixels
	for (int i = 0; i < height;i++){
		for (int j = 0; j < width; j++){
			fin.read(reinterpret_cast<char*>(&BGR), sizeof(pixelValues));
			numBytes+=3;
			pixels[posCursor] = BGR.red;
			pixels[posCursor+1] = BGR.green;
			pixels[posCursor+2] = BGR.blue;
			posCursor += 3;

			//moves cursor to steop over padding
			if(j == width-1){
				fin.seekg((fin.cur + fileHeader.bfOffBits + numBytes + padding) - 1);
				numBytes += padding;
			}
		}
	}

	fin.close();

	return pixels;
}

void BMP_Handler::saveBMP(const char* BMP, const unsigned char* pixels, int width, int height){
	std::ofstream fout(BMP, std::ios::binary);	

	//finds value for padding 
	int padding = 4 - (width*3)%4;
	if (padding == 4) 
		padding = 0;

	//fill in values that are in file header struct
	BMPfileHeader fileHeaderWrite;
	fileHeaderWrite.bfType = 19778;
	fileHeaderWrite.bfSize = ((width*height)*3)+sizeof(BMPfileHeader) + sizeof(BMPCoreHeader) +(padding*height);
	fileHeaderWrite.bfReserved1 = 0;
	fileHeaderWrite.bfReserved2 = 0;
	fileHeaderWrite.bfOffBits = sizeof(BMPfileHeader) + sizeof(BMPCoreHeader);
	fout.write(reinterpret_cast<char*>(&fileHeaderWrite), sizeof(BMPfileHeader));

	//fill in values that are in core header struct
	BMPCoreHeader CoreHeaderWrite;
	CoreHeaderWrite.biSize = sizeof(BMPCoreHeader);
	CoreHeaderWrite.biWidth = width;
	CoreHeaderWrite.biHeight = height;
	CoreHeaderWrite.biPlanes = 1;
	CoreHeaderWrite.biBitCount = 24;
	CoreHeaderWrite.biCompression = 0;
	CoreHeaderWrite.biSizeImage = fileHeaderWrite.bfSize-fileHeaderWrite.bfOffBits;
	CoreHeaderWrite.biXPelsPerMeter = 0;
	CoreHeaderWrite.biYPelsPerMeter = 0;
	CoreHeaderWrite.biClrUsed = 0;
	CoreHeaderWrite.biClrImportant = 0;
	fout.write(reinterpret_cast<char*>(&CoreHeaderWrite), sizeof(BMPCoreHeader));

	pixelValues BGR;
	int numBytes = 0;
	int posCursor = 0;

	//go through pixel array and write to BMP
	for (int i = 0; i < height;i++){
		for (int j = 0; j < width; j++){

			//fill in pixel struct
			BGR.blue = pixels[posCursor+2];
			BGR.green = pixels[posCursor+1];
			BGR.red = pixels[posCursor];

			posCursor += 3;
			fout.write(reinterpret_cast<char*>(&BGR), sizeof(pixelValues));
			numBytes+=3;
			//write padding bits to the BMP
			if(j == width-1){
				for (int k = 0; k < padding; k++){
					fout.write(reinterpret_cast<char*>("\0"), sizeof(char));
				}
				numBytes += padding;
			}
		}
	}

	fout.close();
}